DROP DATABASE IF EXISTS JobManagerDB;
CREATE DATABASE JobManagerDB;
USE JobManagerDB;

DROP TABLE IF EXISTS TaskMessage;
DROP TABLE IF EXISTS TaskRoom;
DROP TABLE IF EXISTS TaskPerson;
DROP TABLE IF EXISTS Message;
DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS Task;
DROP TABLE IF EXISTS Room;

CREATE TABLE Person (
personID int NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
isAdmin tinyint(1) NOT NULL,
password varchar(30),
PRIMARY KEY (personID)
) ENGINE=InnoDB;

CREATE TABLE Room (
roomID int NOT NULL AUTO_INCREMENT,
number varchar(10) NOT NULL,
description varchar(50) NOT NULL,
PRIMARY KEY (roomID)
) ENGINE=InnoDB;

CREATE TABLE Task (
taskID int NOT NULL AUTO_INCREMENT,
description varchar(255) NOT NULL,
isCompleted tinyint(1) NOT NULL,
timeCreated datetime NOT NULL,
timeCompleted datetime,
roomID int NOT NULL,
urgency varchar(20),
isArchived tinyint(1) NOT NULL,
PRIMARY KEY (taskID),
FOREIGN KEY (roomID) REFERENCES Room (roomID)
) ENGINE=InnoDB;

CREATE TABLE Message (
messageID int NOT NULL AUTO_INCREMENT,
personID int NOT NULL,
message varchar(255) NOT NULL,
time datetime NOT NULL,
PRIMARY KEY (messageID),
FOREIGN KEY (personID) REFERENCES Person (personID)
) ENGINE=InnoDB;

CREATE TABLE TaskPerson (
taskID int NOT NULL,
personID int NOT NULL,
KEY taskID (taskID),
KEY personID (personID),
FOREIGN KEY (taskID) REFERENCES Task (taskID),
FOREIGN KEY (personID) REFERENCES Person (personID)
) ENGINE=InnoDB;

CREATE TABLE TaskMessage (
taskID int NOT NULL,
messageID int NOT NULL,
KEY taskID (taskID),
KEY messageID (messageID),
FOREIGN KEY (taskID) REFERENCES Task (taskID),
FOREIGN KEY (messageID) REFERENCES Message (messageID)
) ENGINE=InnoDB;

CREATE TABLE TaskRoom (
taskID int NOT NULL,
roomID int NOT NULL,
KEY taskID (taskID),
KEY roomID (roomID),
FOREIGN KEY (taskID) REFERENCES Task (taskID),
FOREIGN KEY (roomID) REFERENCES Room (roomID)
) ENGINE=InnoDB;
