<?php
function IsEmpty($v){
   return (!isset($v) || trim($v)==='');
}

if (empty($_GET) || IsEmpty($_GET['description']) || IsEmpty($_GET['person'])){
   header('Location: ../index.php');
   exit();  
}


if (!file_exists('data/test.xml')){
   echo '<p>Failed to find schedule xml file</p>';
} else {
   $xml = simplexml_load_file('data/test.xml');

   $task = $xml->addChild('task');
   $task->addChild('id', uniqid());
   $task->addChild('time', date('M d H:i'));
   $task->addChild('description', $_GET['description']);
   $task->addChild('person', $_GET['person']);
   $task->addChild('status', 'wait');

   $xml->saveXML('data/test.xml');
   header('Location: ../index.php');
   exit();  
}
?>
