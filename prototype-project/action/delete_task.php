<?php
function IsEmpty($v){
   return (!isset($v) || trim($v)==='');
}

if (empty($_GET) || IsEmpty($_GET['id'])){
   header('Location: ../index.php');
   exit();  
}

if (!file_exists('data/test.xml')){
   echo '<p>Failed to find schedule xml file</p>';
} else {
   $xml = simplexml_load_file('data/test.xml');

   $id = $_GET['id'];
   foreach($xml->children() as $task){
      if ($task->id == $id){
         unset($task[0]);
         break;
      }
   }

   $xml->saveXML('data/test.xml');
   header('Location: ../index.php');
   exit();  
}
?>
