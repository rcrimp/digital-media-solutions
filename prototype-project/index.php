<?php
// if not logged in, go to welcome page
session_start();
if (!isset($_SESSION['login_id'])){
   header('location: welcome.php');
   exit;
}

// if loggend in, go to the right page
$id = $_SESSION['login_id'];

include('action/database.php');
$sql = 'SELECT name, isAdmin, isNurse, isCleaner FROM Person WHERE personID='.$id.';';
$result = mysql_query($sql, $db); 

// check if query had errors
if (!$result) {
   echo "DB Error, unable to query the database\n";
   echo 'MySQL Error: '.mysql_error();
   exit;
}
// check query returned 0 rows
if (empty($result)){
   echo 'something went wrong, loggend in user doesn\'t exist in the database';
   exit;
}

// fetch the only row
$row = mysql_fetch_assoc($result);

// determine where to redirect 
$url = 'interface.php';
if ($row['isAdmin'] == 'T') {
   $url = 'admin.php'; 
}

// clean up and redirect
mysql_free_result($result);
header('location: '.$url);
exit;
?>

