<?php 
session_start();
if (isset($_SESSION['login_id'])){
   header('Location:index.php');
   exit;
}
?>
<script src="resources/scr/hash.js"></script>
<script>
function submitForm(form){
   if (!empty(form['text-password'])){
      form['password'].value = Sha256.hash(form['text-password'].value);
   }
   form['text-password'].value = "";
   return true;
}
function togglePassword(element){
   document.getElementById("password-input").disabled = !element.checked;
}
</script>

<form action="action/create_account.php" method="post" onsubmit="return submitForm(this);">
<legend>Create new user</legend>
   name
   <input name="name" type="text" placeholder="name" pattern=".{3,50}" required title="Between 3 and 50 letters.">
   <br>
   
   password 
   <input id="toggle-password" type="checkbox" name="use-password" onchange="togglePassword(this);">
   <input id="password-input" name="text-password" type="password" placeholder="password" disabled>
   <input name="password" type="hidden" value="">
   <br>

<fieldset>
   <input name="type" type="radio" value="cleaner" checked>
   Cleaner<br>
   <input name="type" type="radio" value="nurse">
   Nurse<br>
   <input name="type" type="radio" value="admin">
   Administrator<br>
</fieldset>
   
   <input type="submit" value="Submit">
</form>
   

