DROP DATABASE IF EXISTS JobManagerDB;
CREATE DATABASE JobManagerDB;
USE JobManagerDB;

DROP TABLE IF EXISTS Person;
DROP TABLE IF EXISTS Task;

CREATE TABLE Person (
personID int NOT NULL AUTO_INCREMENT,
name varchar(50) NOT NULL,
password char(64) NOT NULL,
isAdmin ENUM('T','F') NOT NULL,
isNurse ENUM('T','F') NOT NULL,
isCleaner ENUM('T','F') NOT NULL,
PRIMARY KEY (personID)
) ENGINE=InnoDB;


CREATE TABLE Task (
taskID int NOT NULL AUTO_INCREMENT,
location varchar(255) NOT NULL,
description varchar(255) NOT NULL,
submitter varchar(50) NOT NULL,
contact varchar(50) NOT NULL,
timeSubmitted datetime NOT NULL,
timeCompleted datetime,
state ENUM('available', 'assigned', 'completed', 'archived') NOT NULL,
urgent ENUM('T', 'F') NOT NULL,
personID int,

PRIMARY KEY (taskID),
FOREIGN KEY (personID) REFERENCES Person(personID)
) ENGINE=InnoDB;
